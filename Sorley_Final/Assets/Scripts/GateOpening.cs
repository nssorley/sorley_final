﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateOpening : MonoBehaviour
{
    LeverInteract li;

    private bool GateIsOpen;

    public GameObject leverTrigger;
    public Animator GateSlideOpen;
    public Animator LeverLean;
    void Start()
    {
        GameObject leverTrigger = GameObject.FindGameObjectWithTag("Lever");
        li = leverTrigger.GetComponent <LeverInteract>();
        GateIsOpen = false;
    }

    
    void Update()
    {
        if (li.IsThisLeverPulled == true)
        {
            GateIsOpen = true;
        }
        if (GateIsOpen == true)
        {
            LeverLean.SetBool("isPulled", true);
            GateSlideOpen.SetBool("GateSlideUp", true);
        }
    }


}
