﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformCrumble : MonoBehaviour
{
    public Transform player;
    Rigidbody rigidBody;
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.useGravity = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            rigidBody.useGravity = true;
        }

    }
}
