﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeSetter : MonoBehaviour
{
    PlayerMovement pm;

    
    public Transform player;
    public Animator rope;
    private bool IsPlayerPresent = false;

    private void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        pm = player.GetComponent<PlayerMovement>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            IsPlayerPresent = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            IsPlayerPresent = false;
        }
    }

    private void Update()
    {
        if (IsPlayerPresent == true)
        {
            
            if (Input.GetButtonDown("Interact"))
            {
                if (pm.IsRopePicked == true)
                {
                    rope.SetBool("RopeIsPlaced", true);
                }
            }
        }

        
    }
}
