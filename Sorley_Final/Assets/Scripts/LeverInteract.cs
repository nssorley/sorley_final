﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverInteract : MonoBehaviour
{
    public Transform player;
    public bool IsThisLeverPulled;
    private bool IsPlayerPresent;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            IsPlayerPresent = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            IsPlayerPresent = false;
        }
    }

    private void Update()
    {
        if (IsPlayerPresent == true)
        {
            if (Input.GetButtonDown("Interact"))
            {
                IsThisLeverPulled = true;
            }
        }
    }
}
