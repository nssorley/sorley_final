﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    CharacterController characterController;
    public float movementSpeed = 5.0f;
    public Transform flower;
    public Transform rope;
    public Transform pit;
    public bool IsRopePicked = false;

    private Vector3 moveDirection = Vector3.zero;

    void Start()
    {
        characterController = GetComponent<CharacterController>();

    }
    void Update()
    {
        if (characterController.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection = moveDirection * movementSpeed;
        }

        //Gravity
        moveDirection.y -= 10f * Time.deltaTime;

        characterController.Move(moveDirection * Time.deltaTime);

    
    
     
    }

    private void OnTriggerEnter (Collider other)
    {
        if (other.transform == flower)
        {
            other.gameObject.SetActive(false);
            SceneManager.LoadScene("CaveofOrigin");
        }
        
        if (other.transform == rope)
        {
            other.gameObject.SetActive(false);
            IsRopePicked = true;
        }

        if (other.transform == pit)
        {
            SceneManager.LoadScene("CaveofOrigin");
        }

    }
}
